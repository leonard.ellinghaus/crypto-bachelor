# Cryptography Bachelor Project: Client Server Registration via the Okamoto Identification Scheme

This Project is a proof-of-concept implementation of an authorization system between a client and a server based on the okamoto protocoll. 
It allows the client to both register to the server and use the registered credentials to login to the system. The difference to all common authorization systems is that in order to identify the client the server uses the okamoto identification protocoll.
As a proof-of-concept this project ceates a low level implementation using sockets to communicate between server and client.
Only two not build-in moduls are used in the application: Pandas and Cryptography

## Starting the Program

In order for the program to run both the server and client need to be run seperately. Both the client and server require a IP and Port as a command line argument. For the server the IP and Port is the address which he will listen for connections on. For the client the IP and Port is the address he will connect to. Therefore both need to be given the same IP and Port.
An example to start both programs is:
> python3 server.py localhost 5000
> python3 client.py lochalhost 5000

## Controlling the client

After having been started the client can be controlled over the command line. Typing "help" will open up a list of all possible commands. The possible commands are:

- register
- login
- help
- quit

both register and login will ask for the username as well as the password in order to register the user/identify oneself to the server. "quit" will simply close the application.

## Use of not build-in modules

As a proof-of-concept project the application is intended to be rather low-level and not use too many external modules. However as to not skew the focus of the project the external modules Pandas and Cryptography are employed in order to manage data storage and the digital signatures respectively. As both functionalities are required yet not the focus of the project. 

## Version requirements

This project requires and is tested using Python version 3.7.7 as well as pandas version 1.2.4 and cryptography version 36.0.0

Both pandas and cryptography can be installed via pip:
> pip install cryptography
> pip install pandas


## Runnuning unittests

In order to run the unittests execute the following command in the test folder:
> python -m unittest
