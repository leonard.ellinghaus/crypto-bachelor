import unittest
import sys

sys.path.insert(0, "../app/shared")
import signature


class TestSignature(unittest.TestCase):
    def test_sign(self):
        msg = "This is the test message used to test the sign"
        certificate = signature.create_cert(msg, "../app/server/private_key.pem")
        self.assertEqual(signature.validate_signature(certificate, msg, "../app/public_key.pem"), True)
        self.assertEqual(signature.validate_signature(certificate, "This is a differnt message", "../app/public_key.pem"), False)
        self.assertEqual(signature.validate_signature(b"This is a differen certificate", msg, "../app/public_key.pem"), False)
        
        