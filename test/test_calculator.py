
import unittest
import sys
sys.path.insert(0, "../app/shared")
import calculator
import constants

class CalculatorTest(unittest.TestCase):
    
    def test_calc_A(self):
        self.assertEqual(calculator.calc_A(10, 15), (pow(constants.G1, 10, constants.P) * pow(constants.G2, 15, constants.P)) % constants.P)
        self.assertEqual(calculator.calc_A(130, 83), (pow(constants.G1, 130, constants.P) * pow(constants.G2, 83, constants.P)) % constants.P)
        self.assertEqual(calculator.calc_A(39, 50), (pow(constants.G1, 39, constants.P) * pow(constants.G2, 50, constants.P)) % constants.P)
        self.assertEqual(calculator.calc_A(985, 1102), (pow(constants.G1, 985, constants.P) * pow(constants.G2, 1102, constants.P)) % constants.P)
        self.assertEqual(calculator.calc_A(1096, 999), (pow(constants.G1, 1096, constants.P) * pow(constants.G2, 999, constants.P)) % constants.P)


    def test_calc_r(self):
        a1, a2 = 90, 130
        b1, b2 = 18, 37
        c = 60
        r1, r2 = calculator.calc_R(a1,a2,b1,b2,c)
        self.assertEqual(calculator.verify_r1_r2(r1,r2,c,calculator.calc_A(b1,b2), calculator.calc_A(a1,a2)), True)
        self.assertEqual(calculator.verify_r1_r2(r1-1,r2,c,calculator.calc_A(b1,b2), calculator.calc_A(a1,a2)), False)


if __name__ == '__main__':
    unittest.main()