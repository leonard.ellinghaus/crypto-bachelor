import argparse
import socket
import sys
import pandas as pd
import os
import ast
from threading import Thread

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

sys.path.insert(0, "../shared")
import calculator
import constants
import signature
from sock_functions import recv_message, send_message, recv_certificate

"""
Implements all functions the server has to provide.
That is handling transmission by registering or identifying users as well as giving out certificates upon request.
"""


def handle_transmission(in_socket):
    """
    receives a transmission via the given socket and starts the appropriate routine.
    Args:
        in_socket (Socket): the socket which is supposed to receive the message
    """
    msg = recv_message(in_socket)
    print("Incoming transmission: {}".format(msg))    
    if msg == "REGISTER":
        register_user(in_socket)
    elif msg == "IDENTIFY":
        okamoto_reciver(in_socket)
    elif msg == "REQUEST":
        give_certificate(in_socket)
    else:
        in_socket.send(b"ERROR [MESSAGE NOT VALID]")
    print("Communication finished.")
    in_socket.close()


def register_user(in_socket):
    """
    Started when a user has requested to be registed within handle_transmission.
    Receives ID and A from the user and then replies with the signature created if all tranmissions worked as expected.
    Will terminate early if messages are not as expected.


    Args:
        in_socket (Socket): the socket that receives the messages. 
    """
    print("Registering new user")
    send_message(in_socket, b"ack")

    # check if local storage exists already, else create one.
    if os.stat("server_localstorage.csv").st_size == 0:
        storage = pd.DataFrame(columns=["ID", "A", "Certificate"])
    else:    
        storage = pd.read_csv("server_localstorage.csv")

    id = recv_message(in_socket)
    if not id:
        print("id has not been send")
        send_message(in_socket, b"ERROR [ID has not been send]")

    print("User ID: {}".format(id))
    if not storage.loc[storage["ID"] == id].empty:
        send_message(in_socket, b"ERROR [USERNAME ALREADY TAKEN]")
        return


    send_message(in_socket, b"ack")

    A = recv_message(in_socket)
    if not A:
        print("Public Version of secret key not send")
        send_message(in_socket, b"ERROR [A has not been send]")
    print("Public version of seceret key: {}".format(A))
    

    my_signature = signature.create_cert(id + A, "private_key.pem")
    print("Signature: {}".format(my_signature))
    send_message(in_socket, my_signature)

 
    new_row = {'ID':id, "A":A, "Certificate":my_signature}
    storage = storage.append(new_row, ignore_index=True)
    storage = storage.drop_duplicates()
    storage.to_csv("server_localstorage.csv", index=False)


    #save_to_storage("server_localstorage.csv", id + "," + A + "," + my_signature)

    return


def okamoto_reciver(in_socket):
    """
    Identifies a user using the Okamoto Identification Scheme. 
    Data is transmitted according to the Scheme.



    Args:
        in_socket (Socket): the socket that receives the messages. 
    """
    print("Identifying user")
    send_message(in_socket, b"ack")

    id = recv_message(in_socket)
    print("ID: {}".format(id))
    send_message(in_socket, b"ack")
    A = recv_message(in_socket)
    print("A: {}".format(A))
    send_message(in_socket, b"ack")
    certificate = recv_certificate(in_socket)
    print("Certificate: {}".format(certificate))
    if signature.validate_signature(certificate, id + A, "../public_key.pem"):
        send_message(in_socket, b"ack")
    else:
        send_message(in_socket, b"ERROR [Certificate was not valid]")
        return

    B = recv_message(in_socket)
    print("B: {}".format(B))
    c = calculator.choose_c()
    send_message(in_socket, bytes(str(c), "utf-8"))
    
    r1_r2 = recv_message(in_socket)
    r1_r2 = r1_r2.split(" ")
    r1 = r1_r2[0]
    r2 = r1_r2[1]
    print("r1 and r2: {} {}".format(r1, r2))
    if r1.isdigit() and r2.isdigit():
        if calculator.verify_r1_r2(int(r1), int(r2), c, int(B), int(A)):
            send_message(in_socket, b"ACCEPTED")
        else:
            send_message(in_socket, b"ERROR [R1 AND R2 NOT CORRECT]")

    return


def give_certificate(in_socket):
    """
    Handles the request to hand out a certificate to a user.
    Receives both ID and A from the user and checks if a signature corresponding to these values is present in storage.
    Args:
        iin_socket (Socket): the socket that receives the messages.
    """
    
    print("Giving out certificate")
    send_message(in_socket, b"ack")

    id = recv_message(in_socket)
    if not id:
        print("id has not been send")
        send_message(in_socket, b"ERROR [ID has not been send]")

    print("User ID: {}".format(id))
    send_message(in_socket, b"ack")

    A = recv_message(in_socket)
    if not A:
        print("Public Version of secret key not send")
        send_message(in_socket, b"ERROR [A has not been send]")
    print("Public version of secret key: {}".format(A))

    if os.stat("server_localstorage.csv").st_size == 0:
        send_message(in_socket, b"ERROR [Certificate not on server]")
        return
    else:    
        storage = pd.read_csv("server_localstorage.csv")
    
    storage.astype({"Certificate":bytes})
    certificate = None
    select_row = storage.loc[(storage["ID"]==id) & (storage["A"] ==A)]
    if not select_row.empty:
        certificate = select_row.iat[0,2]
        certificate = ast.literal_eval(certificate)
    
    if not certificate:
        send_message(in_socket, b"ERROR [Certificate not on server]")
    else:
        send_message(in_socket, bytes(certificate, "utf-8"))
    return



if __name__ == '__main__':
    """
    Sets up the command line arguments as well as waits for clients to connect and then starts handling the transmission
    """
    parser = argparse.ArgumentParser(description="Initialize the trusted authority")
    parser.add_argument("ip", help="The IP the server will bind to")
    parser.add_argument('port', type=int, help="The Port the sever will bind to")
    args = parser.parse_args()

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
    s.bind(("localhost", args.port))
    s.listen(socket.SOMAXCONN)

    try:
        while True:
            (inSocket, conInfo) = s.accept()
            #print("Starting thread...")
            new_thread = Thread(target=handle_transmission, args=(inSocket,))
            new_thread.start()
    finally:
        s.close()
