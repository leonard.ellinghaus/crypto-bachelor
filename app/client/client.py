
import argparse
import socket
import time
import math
import pandas as pd
import os
import ast 

import sys
sys.path.insert(0, "../shared")
import constants 
from sock_functions import connect_to_server, send_message, recv_message, recv_certificate
from calculator import calc_A, pw_to_numbers, calc_R, choose_b1_b2

"""
Implements all functions the client need. Being able to register and login as well as requesting a certificate
"""

def okamoto_sender(s, address, id, A, certificate, a1, a2):
    """
    Implements the identifying side of the Okamoto Identification Scheme. 
    Sends all information needed and then responds to the challenge issued by the server.

    Args:
        s (Socket): the socket that is used to send and receive messages.
        address (tuple): IP and Port of the server
        id (String): ID of the user
        A (int): The public version of the private key
        certificate (bytes): the signature of the users certificate
        a1 (int): first part of the secret key
        a2 (int): second part of the secret key

    Returns:
        String: the string accepting the identification. If the identification fail nothing is returned. 
    """
    s = connect_to_server(s, address)
    if not s:
        return
    

    my_sock_info = s.getsockname()
    remote_sock_info = s.getpeername()
    print("Connection: {} , {}".format(my_sock_info, remote_sock_info))

    has_send = send_message(s, b"IDENTIFY")
    if not has_send:
        print("Message could not be send")
        return

        
    
    msg = recv_message(s)
    if msg == "ack":
        send_message(s, bytes(id, "utf-8"))
    else:
        print("Send was not acknowleged.")
        print(msg)
        return

    msg = recv_message(s)
    if msg == "ack":
        send_message(s, bytes(str(A), "utf-8"))
    else:
        print("Send was not acknowleged.")
        print(msg)
        return

    msg = recv_message(s)
    if msg == "ack":
        send_message(s, certificate)
    else:
        print("Send was not acknowleged.")
        print(msg)
        return


    msg = recv_message(s)
    if msg == "ack":
        b1, b2 = choose_b1_b2()
        B = calc_A(b1, b2)
        send_message(s, bytes(str(B), "utf-8"))
    else:
        print("Certificate was not acknowleged.")
        print(msg)
        return

    c = recv_message(s)
    if c.split(' ')[0] == "ERROR":
        print("No c has been send.")
        print(c)
        return
    if not c.isdigit():
        print("The challenge has not been posed properly")
        print(c)
        return
    
    r1, r2 = calc_R(a1, a2, b1, b2, int(c))

    send_message(s, bytes(str(r1) + " " + str(r2), "utf-8"))

    verdict = recv_message(s)
    if verdict == "ACCEPTED":
        print("Succesfully identified oneself to server")
        return verdict
    else:
        print("Verification to Server failed. r1 and r2 were not correct.")
    return 

    


def register_at_server(s, address, id, A):
    """
    Registers the user by receiving the signature after sending both the ID and A to the server 

    Args:
        s (Socket): the socket used to send and receive the messages
        address (tuple): IP and Port of the server
        id (String): The ID of the user
        A (int): The public version of the private key

    Returns:
        bytes: the signature given by the server. If the registration fails nothing is returned
    """
    s = connect_to_server(s, address)
    if not s:
        return

    my_sock_info = s.getsockname()
    remote_sock_info = s.getpeername()
    print("Connection: {} , {}".format(my_sock_info, remote_sock_info))

    has_send = send_message(s, b"REGISTER")
    if not has_send:
        print("Message could not be send")
        return

    msg = recv_message(s)

    if msg == "ack":
        send_message(s, bytes(id, "utf-8"))
    else:
        print("Send was not acknowleged.")
        print(msg)
        return

    msg = recv_message(s)

    if msg == "ack":
        send_message(s, bytes(str(A), "utf-8"))
    else:
        print("Send was not acknowleged.")
        print(msg)
        return

    
    server_signature = recv_certificate(s)

    if server_signature:        
        print("Registration finished as intended")
    else: 
        print("Certificate was not send")

    return server_signature

def request_certificate(s, address, id, A):
    """
    Requests a certificate from a already registered user from the server using ID and A

    Args:
        s (Socket): the socket used to send and receive the messages
        address (tuple): IP and Port of the server
        id (String): The ID of the user
        A (int): The public version of the private key
    Returns:
        bytes: the signature given by the server. If the request fails nothing is returned
    """
    s = connect_to_server(s, address)
    if not s:
        return

    my_sock_info = s.getsockname()
    remote_sock_info = s.getpeername()
    print("Connection: {} , {}".format(my_sock_info, remote_sock_info))

    has_send = send_message(s, b"REQUEST")
    if not has_send:
        print("Message could not be send")
        return

    msg = recv_message(s)

    if msg == "ack":
        send_message(s, bytes(id, "utf-8"))
    else:
        print("Send was not acknowleged.")
        print(msg)
        return

    msg = recv_message(s)

    if msg == "ack":
        send_message(s, bytes(str(A), "utf-8"))
    else:
        print("Send was not acknowleged.")
        print(msg)
        return

    server_signature = recv_certificate(s)

    if not server_signature:
        print("Certificate could not be recieved!")
        return
    try:
        if server_signature.decode("utf-8").split(" ")[0] == "ERROR":
            print("Problem recieving certificate")
            print(server_signature.decode("utf-8"))
            return  
    except Exception:
        return server_signature

    return server_signature






if __name__ == '__main__':
    """
    Sets up the command line arguments for the client.
    Furthermore it handles the user inputs from the command line in order to login or register.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("ip", help="The IP the client will bind to")
    parser.add_argument("port", help="The Port the client will bind to", type=int)

    args = parser.parse_args()
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)
    sock_addr = (args.ip, args.port)

    print("Welcome to the Okamoto Scheme Identification Client!")
    print("You are connecting to the Server at ({},{})".format(args.ip, args.port))
    print("To quit just write quit.")
    print("For additional help type help.")
    print("Do you want to register or login or quit?")
    print("-----------------------------------------")

    while True:  
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)  
        print("> ", end="")
        command = input()
        if command == "quit":
            s.close()
            break
        elif command == "register":
            print("Please enter your Name: ")
            print("> ", end="")
            name = input()
            print("-----------------------------------------")
            print("Please enter your Password: ")
            print("> ", end="")
            pw = input()
            print("-----------------------------------------")
            

            a1, a2 = pw_to_numbers(pw)
            A = calc_A(a1, a2)
            print("Attempting to connect to Server...")

            server_signature = register_at_server(s, sock_addr, name, A)

            if server_signature:
                #print(server_signature)
                if os.stat("client_localstorage.csv").st_size == 0:
                    storage = pd.DataFrame(columns=["Name", "Pw", "Certificate"])
                else:    
                    storage = pd.read_csv("client_localstorage.csv")
                
                new_row = {'Name':name, "Pw":pw, "Certificate":server_signature}
                storage = storage.append(new_row, ignore_index=True)
                storage = storage.drop_duplicates()
                storage.to_csv("client_localstorage.csv", index=False)

            else:
                print("Registration did not finish as intended")
            s.close()
            print("-----------------------------------------")
        elif command == "login":
            print("Please enter your Name: ")
            print("> ", end="")
            name = input()
            print("-----------------------------------------")
            print("Please enter your Password: ")
            print("> ", end="")
            pw = input()
            print("-----------------------------------------")
            
            storage = pd.read_csv("client_localstorage.csv")
            storage.astype({"Certificate":bytes})

            certificate = None
            select_row = storage.loc[(storage["Name"]==name) & (storage["Pw"] ==pw)]
            if not select_row.empty:
                certificate = select_row.iat[0,2]
                certificate = ast.literal_eval(certificate)
            a1, a2 = pw_to_numbers(pw)
            A = calc_A(a1, a2)
            if not certificate:
                print("Certificate not in Local device. Attempting to receive from server...")
                certificate = request_certificate(s, sock_addr, name, A)

            #Still no certificate    
            if not certificate:
                print("Certificate not present locally or on server. Try Registering to receive one!")
            else:
                okamoto_sender(s, sock_addr, name, A, certificate, a1, a2)

            s.close() 
            print("-----------------------------------------")
        #elif command == "test":
        #    print("Please enter your Password: ")
        #    print("-----------------------------------------")
        #    print("> ", end="")
        #    pw = input()
        #    a1, a2 = pw_to_numbers(pw)

        elif command == "help":
            print("Possible commands: ")
            print("register:  Register to the server with a name and password")
            print("login: Identify yourself to the server using name and password")
            print("quit: close the application")
            print("-----------------------------------------")

        else:
            print("Command unknown type help for a list of all possible commands")
            print("-----------------------------------------")