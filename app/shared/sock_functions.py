import socket
import time
"""
Extends socket functionality in order to make data transfer easier. 
"""

def connect_to_server(s, address):
    """
    Connects the given socket to a other socket listening at the given address
    Args:
        s (Socket): the socket that is used to connect to the address
        address (tuple): the address of the destination. Split in IP and Port

    Returns:
        Socket: the socket after having connected
    """
    i = 0
    while True:
        try:
            s.connect(address)
            return s
        except Exception:
            if i >= 10:
                print("Connection with {} could not be made".format(address))
                return 
            print("Connection could not be made. Trying again...")
            i += 1
            time.sleep(3)

    
def send_message(s, msg):
    """
    sends a message via a already connected socket.
    Args:
        s (Socket): the connected socket
        msg (bytes): the message to be send. Encoded as bytes

    Returns:
        bool: True if the sending was successful. False if not.  
    """
    send = False
    tries = 0
    while not send:
        try:
            s.send(msg)
            send = True
        except Exception:
            print("Message could not be send. Waiting 10 sec...")
            time.sleep(10)
            tries += 1
            if tries > 10:
                break
    return send
    
def recv_message(s):
    """
    receives a message being send to the already connected socket and encodes it as utf-8

    Args:
        s (Socket): the socket that is supposed to receive the message.

    Returns:
        String: the message encoded as utf-8
    """
    try:
        bmsg = s.recv(1000)
        msg = bmsg.decode("utf-8")
        return msg
    except Exception:
        print("Return Mesage could not be received")
        return
        
def recv_certificate(s):
    """
    receives a message being send to the already connected socket and returns it as bytes
    Args:
        s (Socket): the socket that is supposed to receive the message.

    Returns:
        bytes: the bytes that were received
    """
    try:
        bmsg = s.recv(1000)
        return bmsg
    except Exception:
        print("Return Mesage could not be received")
        return
    
    
