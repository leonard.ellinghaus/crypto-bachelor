from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding


"""
Handles both creating the signature as well as the verification of said signature.
"""

def create_cert(msg, path_to_private_key):
    """
    Creates the signature for a specific message using a private RSA Key.

    Parameters
    ----------
    msg : String
        the message that is being signed
    path_to_private_key: String
        the path in order to load the private key that is used to sign

    Returns
    -------
    bytes
        The signature of the message
    """
    with open(path_to_private_key, "rb") as key_file:
        private_key = serialization.load_pem_private_key(
            key_file.read(),
            password=None,
        )
    msg = bytes(msg, "utf-8")
    signature = signature = private_key.sign(
        msg,
        padding.PSS(
            mgf=padding.MGF1(hashes.SHA256()),
            salt_length=padding.PSS.MAX_LENGTH
        ),
        hashes.SHA256()
    )
    return signature


def validate_signature(certificate, document, path_to_public_key):
    """
    Validates the signature of a specific message using a corresponding public RSA key.

    Parameters
    ----------
    certificate : bytes
        The signature of the given message
    document : String
        The message that has supposedly been signed
    path_to_public_key : String
        The path in order to load the public RSA key

    Returns
    -------
    bool
        True if the signature is valid. False if not.
    """
    with open(path_to_public_key, "rb") as key_file:
        public_key = serialization.load_pem_public_key(
            key_file.read(),
        )
    try:   
        public_key.verify(
            certificate,
            bytes(document, "utf-8"),
            padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            hashes.SHA256()
        )
    except Exception:
        return False

    return True
