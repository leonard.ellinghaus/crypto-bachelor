"""
These are the constants used by the project.
Specifically these are the public values used in order to initialize the Okamoto Identification Scheme

P : int
    Modulo Number used
D : int
    Size of the group
G1 : int
    First generator of the group
G2 : int
    Second generator of the group

"""

P = 122503
D = 1201
G1 = 60497
G2 = 17163