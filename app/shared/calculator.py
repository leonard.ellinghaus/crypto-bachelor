import constants as constants
import os
import secrets
import hashlib

"""
This Module is used for the specific calculations needed for the Okamoto Identificationscheme. 
Specifically it is made for the case that the used Group G is a group or subgroup of integers modulo p.
"""


def verify_r1_r2(r1, r2, c, B, A):
    """
    Verifies if the choice of r1 and r2 given the Values c, B and A is correct.
    Specifically it checks if B*A^c = g1^r1 * g2^r2 modulo p 

    Parameters
    ----------
    r1 : int
        one of the two r values to be checked
    r2 : int
        the other r value
    c : int 
        the challenge posed in the Okamoto Scheme
    B : int 
        the public version of the one time key used in the Scheme
    A : int 
        the public version of the secret key 

    Returns
    -------
    bool
        True if r1 and r2 are valid. False otherwise

    """
    val1 = (B * pow(A, c, constants.P)) % constants.P
    val2 = (pow(constants.G1, r1, constants.P) * pow(constants.G2, r2, constants.P)) % constants.P
    return val1 == val2

def pw_to_numbers(pw):
    """
    Turns a password into two numbers. The choice of numbers is deterministic yet save from collisions.

    Parameters
    ----------
    pw : String
        The Password that is turned into the two values

    Returns
    -------
    int
        the first value
    int 
        the second value

    """
    hashed_pw = hashlib.sha256(pw.encode("utf-8")).digest()
    #print(hashed_pw)
    first = hashed_pw[:len(hashed_pw)//2]
    second = hashed_pw[len(hashed_pw)//2:]
    a1 = int(hashlib.sha1(first).hexdigest(), 16) % constants.D
    a2 = int(hashlib.sha1(second).hexdigest(), 16) % constants.D

    #print(a1)
    #print(a2)
    return a1, a2

def calc_A(a1, a2):
    """
    This function is used to calculate the public version of the secret key a1, a2. 
    This is also used to calculate the public version of the one time key b1, b2.

    Parameters
    ----------
    a1 : int
        The first part of the secret key
    a2: int 
        The second pert of the secret key

    Returns
    -------
    int
        The public version of the key
    
    """
    return (pow(constants.G1, a1, constants.P) * pow(constants.G2, a2, constants.P)) % constants.P

def calc_R(a1, a2, b1, b2, c):
    """
    Calculates the values of r1 and r2 that pass the check by verify_r1_r2.

    Parameters
    ----------
    a1 : int
        the first part of the secret key
    a2 : int 
        the second part of the secret key
    b1 : int
        the first part of the one time key
    b2 : int
        the second part of the one time key
    c : int
        the challenge posed to the user

    Returns
    -------
    int
        the first value of r
    int
        the second value of r

    """
    r1 = (b1 + a1*c) % constants.D 
    r2 = (b2 + a2*c) % constants.D
    return r1, r2

def choose_b1_b2():
    """
    Chooses two random numbers within the range given by the group.
    The numbers are choosen cryptographically save.

    Returns
    -------
    int
        the first number
    int
        the second number
    """
    b1 = secrets.randbelow(constants.D)
    b2 = secrets.randbelow(constants.D)
    return b1, b2

def choose_c():
    """
    Chooses one random number within the range given by the group.
    The number is chosen cryptographically save.

    Returns
    -------
    int
        the number
    """
    return secrets.randbelow(constants.D)